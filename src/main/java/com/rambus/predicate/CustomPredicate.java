package com.rambus.predicate;

@FunctionalInterface
public interface CustomPredicate<T> {
    /*boolean test(T t);*/

    boolean check(T t);
}

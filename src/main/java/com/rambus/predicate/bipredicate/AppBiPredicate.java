package com.rambus.predicate.bipredicate;

import java.util.function.BiPredicate;

/**
 * BiPredicate represents a predicate function  which is take two arguments and return boolean value   .
 */
public class AppBiPredicate {

    public static void main(String[] args) {


        BiPredicate<Integer, Integer> bi = (x, y) -> x > y;
        System.out.println(bi.test(5, 7));


        BiPredicate<Integer, Integer> eq = (x, y) -> x -2 > y;
        //returns a composed predicate
        System.out.println(bi.test(2, 3));
        System.out.println(bi.and(eq).test(2, 3));
        System.out.println(bi.and(eq).test(8, 3));


        //returns a predicate that represents the logical negation of this predicate.
        System.out.println("******************negate************************************");
        System.out.println(bi.negate().test(2, 3));

        //returns a composed predicate that represents a short-circuiting logical OR of this predicate and another.
        System.out.println("******************OR************************************");
        System.out.println(bi.test(2, 3));
        System.out.println(bi.or(eq).test(2, 3));
        System.out.println(bi.or(eq).test(8, 3));
    }
}

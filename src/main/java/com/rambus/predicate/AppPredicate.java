package com.rambus.predicate;


import com.rambus.util.Gender;
import com.rambus.util.User;
import com.rambus.util.UserFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AppPredicate {

    public static void main(String[] args) {
        Predicate<User> agePredicate = (u) -> u.getAge() > 20;
        Predicate<User> ageAndGenderPredicate = (u) -> u.getAge() < 20 && u.getGender().equals(Gender.MALE);

        List<User> userList = filterUser(UserFactory.getUserList, agePredicate);
        System.out.println("JDK  Default Predicate filter by age list size: " + userList.size());
        for (User in : userList) {
            System.out.println(in);
        }
        System.out.println("****************************************************************");
        System.out.println("JDK  Default Predicate filter by age and gender list size: " + userList.size());

        userList = filterUser(UserFactory.getUserList, ageAndGenderPredicate);
        for (User in : userList) {
            System.out.println(in);
        }
        System.out.println("****************************************************************");
        CustomPredicate<User> customAgePredicate = (u) -> u.getAge() > 20;
        userList = filterCustomUser(UserFactory.getUserList, customAgePredicate);
        System.out.println("Custom Predicate filter by age list size: " + userList.size());
        for (User in : userList) {
            System.out.println(in);
        }

        System.out.println("****************************************************************");
        System.out.println("Custom Predicate filter by age and gender list size: " + userList.size());

        CustomPredicate<User> customAgeAndGenderPredicate = (u) -> u.getAge() > 20 && u.getGender().equals(Gender.FEMALE);
        userList = filterCustomUser(UserFactory.getUserList, customAgeAndGenderPredicate);
        for (User in : userList) {
            System.out.println(in);
        }
        System.out.println("****************************************************************");


    }

    public static List<User> filterUser(List<User> users, Predicate<User> userPredicate) {
        return users.stream().filter(userPredicate).collect(Collectors.toList());
    }

    public static List<User> filterCustomUser(List<User> users, CustomPredicate<User> userPredicate) {
        List<User> newUsers = new ArrayList<>();
        for (User user : users) {
            if (userPredicate.check(user)) {
                newUsers.add(user);
            }
        }
        return newUsers;
    }


}

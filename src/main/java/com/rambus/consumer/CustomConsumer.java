package com.rambus.consumer;

import com.rambus.util.User;
import com.rambus.util.UserFactory;

import java.util.List;
import java.util.function.Consumer;

public interface CustomConsumer {
    void accept(List<User> users);
}

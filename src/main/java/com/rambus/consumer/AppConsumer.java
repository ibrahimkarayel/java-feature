package com.rambus.consumer;

import com.rambus.util.User;
import com.rambus.util.UserFactory;

import java.util.List;
import java.util.function.Consumer;

/*
 * Represents an operation that accepts a single input argument and returns no result
 * */
public class AppConsumer {

    public static void main(String[] args) {
        Consumer<User> consumer = u -> System.out.print(" ->Name:" + u.getName());

        printUserNames(UserFactory.getUserList, consumer);
        //  UserFactory.getUserList.forEach(consumer);

        System.out.println("\n********************************************************************************************************************");
        Consumer<User> consumerWithAndThen = consumer.andThen(u -> System.out.print("-Surname " + u.getSurname()));
        printUserNames(UserFactory.getUserList, consumerWithAndThen);
        //UserFactory.getUserList.forEach(consumerWithAndThen);


        System.out.println("\n*********************************************************************************************************************");
        CustomConsumer customConsumer = users -> {
            for (User u : users) System.out.print(" ->Name:"+ u.getName());
        };
        customConsumer.accept(UserFactory.getUserList);



    }

    public static void printUserNames(List<User> users, Consumer<User> consumer) {
        for (User user : users) {
            consumer.accept(user);
        }
    }


}

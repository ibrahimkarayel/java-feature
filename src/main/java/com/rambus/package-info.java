package com.rambus;

/*
 A lambda expression is:
  an unnamed function with parameters and a body,
  helps us to write our code in functional style,
 It provides a clear and concise way to implement SAM interface(Single Abstract Method)by using an expression.
 It is very useful in collection library in which it helps to iterate,filter and extract data.
 <p>
  <h3>Why use Lambda Expression</h3>
  <ul>
    <li> To provide the implementation of Functional interface.
    <li>Less coding.
  </ul>

 Java lambda expression is consisted of three components.

1) Argument-list: It can be empty or non-empty as well.

2) Arrow-token: It is used to link arguments-list and body of expression.

3) Body: It contains expressions and statements for lambda expression.
*/

package com.rambus.interfaces;

public class Animal {

    public static void main(String[] args) {
        Actions actions = new Bird();
        actions.fly("Bird1");
        actions.move("Bird1");
        actions.stop("Bird1");
        actions.actionDefault();

        // compiler error static method may be invoked on containing interface class only
        /*actions.log();*/
        Actions.log(); //ok

        Bird bird=new Bird();
        bird.stop("Bird2");
        bird.move("Bird2");
        bird.actionDefault();

        Move move = new Cat();
        move.move("Cat1");
        move.stop("Cat1");

        //move.log();         // compiler error static method may be invoked on containing interface class only

    }
}

class Bird implements Actions {
    @Override
    public void fly(String s) {
        System.out.println("Bird flying: " + s);

    }

    @Override
    public void move(String s) {
        System.out.println("Bird moving: " + s);

    }
}

class Cat implements Move {
    @Override
    public void move(String s) {
        System.out.println("Cat moving: " + s);

    }
}

package com.rambus.interfaces;

/**
 * Any interface with a SAM(Single Abstract Method) is a functional interface
 */
public interface Actions extends Fly, Move {

    default void stop(String s) {
        System.out.println(s + ": Stop after any action");
    }

    default void actionDefault(){
        System.out.println("Actions default ");
    }
    static String log() {
        return "Concrete Static Actions";
    }
}

@FunctionalInterface
interface Fly {
    void fly(String s);

    default void stop(String s) {
        System.out.println(s + ": Stop after fly");
    }

    static String log() {
        return "log fly";
    }
}

@FunctionalInterface
interface Move {
    void move(String s);

    default void stop(String s) {
        System.out.println(s + ": Stop after move");
    }

    static String log() {
        return "log move";
    }
}

@FunctionalInterface
interface Dummy {

    void dummy();

    default void stop(String s) {
        System.out.println(s + ": Stop dummy");
    }

    static String log() {
        return "log dummy";
    }
}
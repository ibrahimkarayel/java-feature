package com.rambus.interfaces;

public class Hide {

    public static void main(String[] args) {
        Parent parent = new Child();
        parent.hide();
        parent.sample();
        parent.sampleDefault();

        Child child=new Child();
        child.hide();
        child.sample();
        child.sampleDefault();
    }
}

class Parent implements A,B{
    @Override
    public void sampleDefault() {
        System.out.println("sampleDefault parent");
    }

    static void hide() {
        System.out.println("hide in parent");
    }
    void sample(){
        System.out.println("parent sample");
    }
}

class Child extends Parent {
    /*@Override
    public void sampleDefault() {
        System.out.println("sampleDefault child");
    }*/
    static void hide() {
        System.out.println("hide in child");
    }

    @Override
    void sample(){
        System.out.println("child sample");
    }

}

interface A{
    default  void sampleDefault(){
        System.out.println("A sample default");
    }
    static void hide() {
        System.out.println("hide in A");
    }
}
interface B{
    default  void sampleDefault(){
        System.out.println("B sample default");
    }
    static void hide() {
        System.out.println("hide in B");
    }
}
package com.rambus.util;


public interface Shape {
    double area();
}

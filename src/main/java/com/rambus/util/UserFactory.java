package com.rambus.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserFactory {

    public static List<User> getUserList = new ArrayList<>(Arrays.asList(
            new User("Deniz", "Demir", Gender.MALE, 17),
            new User("Maria", "sharapova", Gender.FEMALE, 26),
            new User("Robin", "Walker", Gender.MALE, 35),
            new User("Charles", "Barkley", Gender.MALE, 18),
            new User("Kobe", "Bryant", Gender.MALE, 29),
            new User("Robın", "Hood", Gender.MALE, 44),
            new User("Anna", "Kendrick", Gender.FEMALE, 19),
            new User("Robert", "Zemeckis", Gender.MALE, 16),
            new User("Alice", "Guy", Gender.FEMALE, 41),
            new User("Pelin", " Esmer", Gender.MALE, 21)
    ));

}

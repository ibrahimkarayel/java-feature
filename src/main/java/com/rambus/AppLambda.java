package com.rambus;

public class AppLambda {

    public static void main(String[] args) {

        // redundant type declaration and return type
        // takes two int parameters and returns the max.
        Max m = (int x, int y) -> {
            int max = x > y ? x : y;
            return max;
        };
        Max max = (a, b) -> a > b ? a : b;

        // takes an int parameter and returns the parameter value incremented by 1.
        Add add = (a) -> a + 1;
        // redundant type declaration and return type
        Add add2 = (int a) -> {
            return a + 1;
        };

        //takes a String parameter and prints it on the standard output.
        // redundant type declaration
        PrintStr printStr = (String msg) -> System.out.println(msg);
        PrintStr printStr1 = msg -> System.out.println(msg);

        //takes no parameters and returns string .
        Print print = () -> "Hello";

    }

}

@FunctionalInterface
interface Max {
    int apply(int a, int b);
}

@FunctionalInterface
interface Add {
    int apply(int a);
}

@FunctionalInterface
interface PrintStr {
    void apply(String s);
}

@FunctionalInterface
interface Print {
    String apply();
}
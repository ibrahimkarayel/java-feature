package com.rambus.function;


public class App {

    public static void main(String[] args) {

        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("run task 1");
            }
        };

        Runnable task2 = () -> System.out.println("run task 2");

        // Bad if you have single line dont use  {}
        Runnable task3 = () -> {
            System.out.println("run task 2");
        };

        CustomRunnable customRunnable = new CustomRunnable() {
            @Override
            public void run() {
                System.out.println("run task for customRunnable");

            }
        };

        CustomRunnable customTask = () -> System.out.println("run task for customRunnable");

        Thread t1 = new Thread(task1);
        t1.start();

        t1=new Thread(task2);
        t1.start();


    }

}

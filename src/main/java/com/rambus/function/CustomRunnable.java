package com.rambus.function;

@FunctionalInterface
public interface CustomRunnable {
    void run();
}

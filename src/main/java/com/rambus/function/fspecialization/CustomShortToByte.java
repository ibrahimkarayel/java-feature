package com.rambus.function.fspecialization;

@FunctionalInterface
public interface CustomShortToByte {
    byte applyAsByte(short s);
}

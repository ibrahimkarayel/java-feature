package com.rambus.function.fspecialization;

import com.rambus.util.Square;

import java.util.function.DoubleFunction;
import java.util.function.IntFunction;
import java.util.function.ToIntFunction;

/**
 *
 * The eight primitive data types supported by the Java programming language are:
 * byte 8 bit signed two's complement integer
 * short 16 bit  signed two's complement integer
 * int 32 bit signed two's complement integer
 * long 64 bit two's complement integer
 * float 32 bit IEEE 754 floating point.
 * double 64 bit IEEE 754 floating point.
 * boolean 1 bit true false
 * char 16 bit Unicode character
 *
 * Primitive Function Specializations Class
 * Since a primitive type can’t be a generic type argument,
 * there are versions of the Function interface for most used primitive types double, int, long, and their combinations in argument and return types:
 * <p>
 * IntFunction, LongFunction, DoubleFunction: arguments are of specified type, return type is parameterized
 * ToIntFunction, ToLongFunction, ToDoubleFunction: return type is of specified type, arguments are parameterized
 * DoubleToIntFunction, DoubleToLongFunction, IntToDoubleFunction, IntToLongFunction, LongToIntFunction, LongToDoubleFunction
 * — having both argument and return type defined as primitive types, as specified by their names
 */


public class FSpecialization {

    public static void main(String[] args) {
        IntFunction<Square> intFunction=(a)->new Square(a,a);
        System.out.println("The area of Square: "+ intFunction.apply(5).area());
        //compiler error expected int as an input
        /*System.out.println("The area of Square: "+ intFunction.apply(5.0).area());*/

        DoubleFunction<Square> doubleFunction=(a)->new Square(a,a);
        System.out.println("The area of Square: "+ doubleFunction.apply(10.0).area());


        CustomShortToByte customShortToByte = (s)-> (byte) s;

        System.out.println(customShortToByte.applyAsByte((short)127));
        System.out.println(customShortToByte.applyAsByte((short)130));

    }

}

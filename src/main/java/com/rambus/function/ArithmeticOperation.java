package com.rambus.function;

@FunctionalInterface
public interface ArithmeticOperation {
    int apply(int i);
}

package com.rambus.function;

import com.rambus.util.Gender;
import com.rambus.util.User;
import com.rambus.util.Util;

import java.util.function.Function;

public class AppFunction {

    public static void main(String[] args) {

        Function<String, String> function = s -> s.toUpperCase();
        System.out.println(function.apply("Hello Function !!! "));
        /*Function objFunc = s -> s.toUpperCase(); //compiler error without T, R only object methods*/
        Function objFunc = s -> s.getClass();
        System.out.println(objFunc.apply("Hello Object !!! "));
        System.out.println(objFunc.apply(new User("Ali", "Adem", Gender.MALE, 30)));


        System.out.println("Custom Arithmetic Operation Functions");
        ArithmeticOperation add = i -> ++i;

     /*  //redundant int type and return statement
        ArithmeticOperation aa = (int i) -> { return i + 1; }; */

        ArithmeticOperation sub = i -> --i;
        ArithmeticOperation square = i -> i * i;
        System.out.println("Add " + add.apply(5));
        System.out.println("Sub " + sub.apply(20));
        System.out.println("Sq " + square.apply(4));
        //System.out.println(square.apply("15")); // compiler error input must be int

        // Function composition
        System.out.println("Add and square: " + add.apply(square.apply(10)));

        CustomFunction<String, Integer> customFunction = s -> s.length();
        System.out.println("Total lenght of sentence: " + customFunction.apply("How many chars and white spaces"));

        /*CustomFunction f = s -> s>20; // compiler error only object methods accepted*/
        CustomFunction objCustFunc = s -> s.equals("me");
        System.out.println(objCustFunc.apply("me"));
        System.out.println(objCustFunc.apply("you"));


        Function<Integer, Integer> addUtil = Util::add;
        Function<String, String> concatUtil = Util::concat;

        System.out.println(addUtil.apply(5));
        System.out.println(concatUtil.apply("1 + "));



    }


}

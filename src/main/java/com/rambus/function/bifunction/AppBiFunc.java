package com.rambus.function.bifunction;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * BiFunction a function that accepts two arguments and produces a result
 */
public class AppBiFunc {

    public static void main(String[] args) {

        BiFunction<String, String, Integer> biFunction = (a, b) -> a.length() + b.length();
        System.out.println(biFunction.apply("Hello", "BiFunction"));

        CustomBiFunction<Integer, Integer, Double> customBiFunction = (a, b) -> Double.valueOf(a + b);
        System.out.println(customBiFunction.apply(10, 12));

        // bad practice {} and return statements
        BiFunction<String, String, String> bi = (x, y) -> {
            return x + y;
        };
        //bi = (x, y) ->  x + y;

        //Returns a composed function that first applies this function to its input, and then applies the after function to the result.
        BiFunction<Integer, Integer, Integer> biF = (x, y) -> x + y;
        Function<Integer, Integer> f = x -> x * 2;
        System.out.println(biF.andThen(f).andThen(f).apply(3, 2));
        // (3*2 + 2*2 )*2

    }
}

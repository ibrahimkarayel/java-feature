##JDK 8 Enhancements

ibrahim KARAYEL -  Software Developer at Rambus

[github.com/ibrahimkarayel](https://github.com/ibrahimkarayel)

INTRODUCTION
------------

In this project include some sample JDK 8 features

1.	Functional interfaces and Lambda expressions
2.	Default methods and Static methods in interface,
3.	Method references,
4.  Java Time API,
5.	Stream API,
6.	Base64 Encode Decode,
7.	Optional class,
8.	Collectors class,
9.	ForEach() method,
10.	Parallel array sorting,
11.	Nashorn JavaScript Engine,
12.	Parallel Array Sorting,
13.	Type and Repating Annotations,
14.	IO Enhancements,
15.	Concurrency Enhancements,
16.	JDBC Enhancements etc.


REQUIREMENTS
------------
JDK 1.8, Maven


INSTALLATION
------------
download project import as a maven project and run each App* class

